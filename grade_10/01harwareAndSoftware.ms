.ps 12
.fam H
.po 1.20c 
.ll 19c 
.de UH
.ps 14
\fB\\$*\fR
.sp
.ps 12
..
.UH 10.1.1 The difference between hardware and software    
Hardware refers to the physical components of a computer system.
This includes devices you can touch and see, such as 
the central processing unit (CPU), memory (RAM), hard drives, 
motherboards, keyboards, monitors, and other peripherals like printers and scanners.

Software, on the other hand, refers to the intangible programs and applications that run on the hardware. 
These are the coded instructions and rules that run the hardware.
This includes the operating system (like Windows, macOS, or Linux), applications (like word processors, games, or web browsers), and system utilities that manage and control hardware functions.

In essence, hardware is what makes up the physical structure of a computer, while software provides the instructions and functionality to perform tasks.
