.\" Comparison Table for Various Devices
.TL
Comparison of Devices
.AU
Your Name
.DD
Date

.SS
Table of Devices
.TS
tab(|);
c | c | c | c | c | c | c | c.
Device Type          | Portability/Mobility | Processing Power      | Operating System          | Example OS               | Application Software       | Standalone Applications         | Network Applications         
_
Laptop               | High                 | Moderate to High      | Windows, macOS, Linux      | Windows 10, macOS Ventura | Office Suite, Web Browsers | Local Database, Media Players   | Email, Cloud Services       
Desktop              | Low                  | High                  | Windows, macOS, Linux      | Windows 10, Ubuntu        | CAD, Graphics Software    | Local Gaming, Productivity Apps | Remote Desktop, File Sharing  
Server               | Low                  | Very High             | Windows Server, Linux Server| Windows Server, CentOS    | Web Servers, Database Servers| Local Hosting, Data Management | Web Hosting, Cloud Services   
Embedded Computer     | Medium               | Low to Moderate       | Real-time OS, Linux        | FreeRTOS, Raspbian        | Custom Applications       | Device Control, Data Logging   | IoT Applications             
Smart Wear           | High                 | Low                   | Proprietary, Android       | Wear OS                   | Fitness Tracking, Notifications| Health Monitoring              | Sync with Mobile Apps        
Tablet               | High                 | Moderate              | iOS, Android               | iPadOS, Android 12        | Media Consumption, Browsing| E-Book Readers, Note Taking   | Video Calls, Cloud Services   
Smartphone           | Very High            | Moderate              | iOS, Android               | iOS, Android 12           | Social Media, Games      | Messaging, Photography         | Internet Browsing, Mobile Apps 
Single Board Computer | Medium              | Low to Moderate       | Linux, Windows             | Raspberry Pi OS, Ubuntu    | DIY Projects, Prototyping | Home Automation, Retro Gaming  | Local Network Projects        
.TE

