# Step-by-Step Java Walkthrough: `Player.java` and `Main.java`

In this tutorial, we'll create two classes: `Player` and `Main`. We’ll introduce each concept gradually, showing why certain programming practices are important, particularly regarding encapsulation and method overriding.

---

## Step 1: **Creating the Basic `Player` Class (Without Setters/Getters)**

First, we'll create a simple `Player` class that holds basic attributes without any access control.

### **Initial `Player.java`**
```java
public class Player {
    // Directly accessible public variables
    public String name;
    public int score;
}
Explanation:
Attributes: name and score are public, meaning they can be accessed directly by any class.

Example:

java
Copy code
Player player1 = new Player();
player1.name = "Steve";  // Directly setting a public attribute
player1.score = 100;
Why this is not ideal:

Allows external classes to modify name and score without control, which may lead to invalid states (e.g., assigning a non-string value).
Step 2: Adding Encapsulation: Using private Variables with Setters and Getters
Now, we'll update the Player class to use private attributes and provide public getters and setters for controlled access.

Updated Player.java with Setters and Getters
java
Copy code
public class Player {
    // Private attributes (cannot be accessed directly from outside)
    private String name;
    private int score;

    // Getter for name
    public String getName() {
        return name;
    }

    // Setter for name with validation
    public void setName(String name) {
        if (name != null && !name.trim().isEmpty()) {
            this.name = name;
        } else {
            System.out.println("Invalid name. Name cannot be null or empty.");
        }
    }

    // Getter for score
    public int getScore() {
        return score;
    }

    // Setter for score with validation
    public void setScore(int score) {
        if (score >= 0) {  // Prevent negative scores
            this.score = score;
        } else {
            System.out.println("Invalid score. Score cannot be negative.");
        }
    }
}
Explanation:
private keyword: Now, name and score can only be accessed within the Player class.
Getter method: Allows other classes to read the values.
Example: player.getName()
Setter method: Controls how the values are modified, ensuring they remain valid.
Example: player.setName("Steve")
Step 3: Using the Main Class to Test Our Player Class
Now, let’s create a Main class to test our Player object and see the validation in action.

Main.java
java
Copy code
public class Main {
    public static void main(String[] args) {
        Player player = new Player();

        // Set valid name
        player.setName("Steve");
        System.out.println("Player Name: " + player.getName());  // Output: Player Name: Steve

        // Attempt to set invalid name
        player.setName("");  // Invalid name
        System.out.println("Player Name: " + player.getName());  // Output: Player Name: Steve (unchanged)

        // Directly trying to set an invalid value (this line will not compile):
        // player.name = 66;  // Uncommenting this line will cause a compile-time error

        // Set valid score
        player.setScore(100);
        System.out.println("Player Score: " + player.getScore());  // Output: Player Score: 100

        // Attempt to set an invalid score
        player.setScore(-10);  // Invalid score
        System.out.println("Player Score: " + player.getScore());  // Output: Player Score: 100 (unchanged)
    }
}
Key Points in Main.java:
Valid Input: When we set the name to "Steve", it works as expected.
Invalid Input: When we attempt to set the name to an empty string, our setter method prevents the change and displays an error message.
Direct Access Denied: The line attempting to set an integer to name is commented out, indicating it would cause a compile-time error if uncommented.
Step 4: Overriding toString() Method
Next, let’s demonstrate how to override the toString() method to provide meaningful output when printing our Player object.

Adding toString() Method to Player.java
java
Copy code
public class Player {
    private String name;
    private int score;

    // Getter and Setter methods remain the same...

    // Overriding toString() method
    @Override
    public String toString() {
        return "Player Name: " + name + ", Score: " + score;
    }
}
Using toString() in Main.java
java
Copy code
public class Main {
    public static void main(String[] args) {
        Player player = new Player();
        player.setName("Steve");
        player.setScore(100);

        // Print the Player object using the overridden toString method
        System.out.println(player);  // Output: Player Name: Steve, Score: 100
    }
}
Explanation of toString() Method:
Implicit Call: When we print player, Java implicitly calls the toString() method on the player object, providing a more meaningful representation.
Custom Output: By overriding toString(), we control what gets printed, which is helpful for debugging and logging.
Summary of the Walkthrough:
Encapsulation: We learned the importance of using private attributes and providing public getters and setters for controlled access.
Data Validation: We implemented validation in the setters to maintain data integrity.
Method Overriding: We explored how to override the toString() method to provide custom output when printing objects.
Complete Code
Here’s the complete code for both files.

Player.java
java
Copy code
public class Player {
    private String name;
    private int score;

    // Getter for name
    public String getName() {
        return name;
    }

    // Setter for name with validation
    public void setName(String name) {
        if (name != null && !name.trim().isEmpty()) {
            this.name = name;
        } else {
            System.out.println("Invalid name. Name cannot be null or empty.");
        }
    }

    // Getter for score
    public int getScore() {
        return score;
    }

    // Setter for score with validation
    public void setScore(int score) {
        if (score >= 0) {  // Prevent negative scores
            this.score = score;
        } else {
            System.out.println("Invalid score. Score cannot be negative.");
        }
    }

    // Overriding toString() method
    @Override
    public String toString() {
        return "Player Name: " + name + ", Score: " + score;
    }
}
Main.java
java
Copy code
public class Main {
    public static void main(String[] args) {
        Player player = new Player();

        // Set valid name
        player.setName("Steve");
        System.out.println("Player Name: " + player.getName());  // Output: Player Name: Steve

        // Attempt to set invalid name
        player.setName("");  // Invalid name
        System.out.println("Player Name: " + player.getName());  // Output: Player Name: Steve (unchanged)

        // Set valid score
        player.setScore(100);
        System.out.println("Player Score: " + player.getScore());  // Output: Player Score: 100

        // Attempt to set an invalid score
        player.setScore(-10);  // Invalid score
        System.out.println("Player Score: " + player.getScore());  // Output: Player Score: 100 (unchanged)

        // Print the Player object using the overridden toString method
        System.out.println(player);  // Output: Player Name: Steve, Score: 100
    }
}

