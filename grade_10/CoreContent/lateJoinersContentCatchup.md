
Theory textbook
pg 1 - 19
pg 74 - 84

Programming
pg 20 - 54 -> Char String int and Maths
pg 83 -101 -> For loops
pg 118 - 145 -> If loops
pg 147 - 168 -> Switch case
pg 171 - 192 -> While loops
pg 193 - 205 -> Nested Loops
pg 206 - 216 -> Methods
pg 219 - 247 -> SQL
