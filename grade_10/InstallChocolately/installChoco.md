markdown

# Setting Up Chocolatey on Windows

## Prerequisites

1. **Windows 10 or higher**: Chocolatey works on Windows 10, Windows Server 2016, and higher versions.
2. **Administrator Access**: You need administrative privileges to install Chocolatey.

## Step-by-Step Installation Guide

### Step 1: Open Command Prompt as Administrator

1. Press `Win + X` to open the Power User menu.
2. Click on **Windows PowerShell (Admin)** or **Command Prompt (Admin)**.
3. If prompted by User Account Control (UAC), click **Yes** to allow.

### Step 2: Install Chocolatey

1. Copy and paste the following command into the command prompt and press `Enter`:

   For PowerShell:
   ```powershell
   Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

For Command Prompt:

cmd

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

    Wait for the installation to complete. You should see messages indicating the progress.

Step 3: Verify Installation

    After the installation completes, close the command prompt and open a new one (as Administrator).

    Type the following command to verify that Chocolatey is installed:

    cmd

    choco -v

    This command should display the version of Chocolatey installed.

Using Chocolatey
Step 1: Install Packages

To install a package, use the following command syntax:

cmd

choco install <package_name>

For example, to install Google Chrome:

cmd

choco install googlechrome

Step 2: Upgrade Packages

To upgrade all installed packages, use:

cmd

choco upgrade all

Step 3: Uninstall Packages

To uninstall a package, use:

cmd

choco uninstall <package_name>

For example, to uninstall Google Chrome:

cmd

choco uninstall googlechrome

Additional Tips

    You can search for packages using:

    cmd

    choco search <search_term>

    For more advanced usage and commands, refer to the Chocolatey Documentation.

sql


You can copy and paste this Markdown into your preferred Markdown editor or viewer. Let me 
