## Java Program structure

At grade 10 level out programs have the following structure.

1. Package declaration - we declare which package this class belongs to.
2. imports - if we need to import a package add this at the top of the file.
3. Instance variable - we declare instance variables here if this is a templating class
4. Main method - the main method is where all the action begins.

