
## Java Syntax
After completing make labels, I hope you are comfortable with the following Java Truths.

1. All expressions end with a semicolon. Since Java ignores white space we must tell it where the expression ends so that it may parse the line correctly.
2. Very rarely if ever will a line contain a ; and a { or a (
3. Curly brackets start a block of code such as a Class or a Method. We often insert line breaks which are ignored by Java in order to make code more readable.
4. Classes always take capital letters such as Animal, Person, System
5. Methods always have a lower case letter unless the method is a constructor.
6. A period separates class methods and packages System.out.println()
