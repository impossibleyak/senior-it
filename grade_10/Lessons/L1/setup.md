Lesson One
Java Setup

Install the following:
    - [Git](https://git-scm.com/download/win)
    - [Java](https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.msi)
    - [Netbeans](https://www.apache.org/dyn/closer.lua/netbeans/netbeans-installers/20/Apache-NetBeans-20r1-bin-windows-x64.exe)
Take note of the versions
    - Java version 17
    - Netbeans version 20

Open a command prompt and run javac and java --version

Installing your git repository

1. Ensure you navigate to your IT folder.
2. Right click and open a Git Shell
3. Type the following "git clone https://gitlab.com/impossibleyak/senior-it.git
4. Navigate through the folder to ensure you have all the materials
5. Running a git pull in the same folder will pull in all the new changes


