
== Strings are not equal ==

Looking at the following code.

String s1 = "HELLO";
        String s2 = "HELLO";
        String s3 =  new String("HELLO");
 
        System.out.println(s1 == s2); // true
        System.out.println(s1 == s3); // false
        System.out.println(s1.equals(s2)); // true
        System.out.println(s1.equals(s3)); // true
    }    
    

When s1 and s2 are created. Java tries to save space by making s1 and s2 into the same object in memory.
Since they are equal in allocation java sees this as efficient.
Both strings are said to be in the pool.
Since these are the same, one being an actual object and another being a reference to the same object, they are equal according to java. These strings 'live' at the same address thus they are equal.

When we use the 'new' keyword we force java to make us a new object, despite making the text the same as the other strings. == comparison gives us an incorrect answer but the equal() method returns the correct answer by comparing every char in the string as integers to succeed in a true match reliably.


