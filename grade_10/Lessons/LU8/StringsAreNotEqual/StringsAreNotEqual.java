
/* Programming Taxonomy

Level 1 - declare variables, instantiate object with no parameters, input, output,
increment, decrement, casting types

Level 2 - create and instantiate object with parameters, calculations, casting
types, if statement with one condition for loops and while loops, known algorithms
(sorting and searching), generate a random number, code simple (no loops or if
statements) void methods in a class with no parameters

Level 3 - Arrays of objects, methods with loops or if statements, code typed/void
methods with parameters and call them correctly. Static and final fields.
*/

public class StringsAreNotEqual 
{
    public static void main(String[] args)
    
{
        String s1 = "HELLO";
        String s2 = "HELLO";
        String s3 =  new String("HELLO");
 
        System.out.println(s1 == s2); // true
        System.out.println(s1 == s3); // false
        System.out.println(s1.equals(s2)); // true
        System.out.println(s1.equals(s3)); // true
    }    
}


