## SQL
SQL (Structured Query Language) is a standard programming language used to manage, query, and manipulate databases. It allows users to interact with relational databases, which store data in tables consisting of rows and columns. SQL is essential for tasks such as retrieving data, updating records, and managing the structure of databases.

Setting up a local SQL server on your machine involves installing and configuring database software that allows you to develop, test, and manage databases locally. A common setup is using MySQL or Microsoft SQL Server as the database management system (DBMS). Below is a general guide for setting up a local SQL server on your computer.


# Step-by-Step Guide for MySQL Server Setup on Localhost

## 1. Download and Install MySQL
- Visit the https://dev.mysql.com/downloads/mysql/
- Download the MySQL Installer (choose the LTS windows version and download an MSI installer).
- You my be prompted to install the Ms Visaul C++ resdistirbutable available here https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170
- 
- During installation, select **"Complete "** (includes MySQL Server, MySQL Workbench, and necessary components).

## 2. Configure MySQL Server
- During installation, set the root password to "root"(admin password for the server).
- Configure the default port (usually 3306).
- Set the server to start automatically when your machine starts.
- Complete the installation, making sure the MySQL Server is successfully installed and running.

## 3. Test MySQL on Localhost
- Open the MySQL command line client using your windows start button and the search function
- login with the password root
- Run the following command to access the MySQL command-line interface (CLI):

    ```bash
    mysql -u root -p
    ```

- You will be prompted to enter the root password you set during installation.
- If successful, you should see the MySQL prompt (`mysql>`), confirming the server is running locally.

## 4. Create a Database
- After accessing the MySQL prompt, you can create a database:

    ```sql
    CREATE DATABASE school;
    ```

## 5. Create a Table in the Database
- Use the following commands to create a table in your `school` database:

    ```sql
    USE school;
    CREATE TABLE students (
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(50),
        age INT,
        grade CHAR(1)
    );
    ```

## 6. Insert Data into the Table
- Add data to your `students` table:

    ```sql
    INSERT INTO students (name, age, grade)
    VALUES ('John Doe', 16, 'A');
    ```

## 7. Query Data from the Table
- Retrieve data to ensure everything is set up correctly:

    ```sql
    SELECT * FROM students;
    ```


# Installing MySQL Connector/J for NetBeans

## 1. Download the MySQL Connector/J

1. **Visit the MySQL Connector/J Download Page**:
   - Go to the [MySQL Connector/J download page](https://dev.mysql.com/downloads/connector/j/).

2. **Select Your Platform**:
   - Choose "platform independent" and download the ZIP or TAR archive.

3. **Extract the Downloaded File**:
   - Once downloaded, extract the archive to a known directory. Inside, you will find a `.jar` file (e.g., `mysql-connector-java-x.x.xx-bin.jar`).

## 2. Install the MySQL Connector/J in NetBeans

1. **Open NetBeans IDE**:
   - Launch your NetBeans IDE.

2. **Create or Open a Project**:
   - Either create a new Java project or open an existing one where you want to use MySQL.

3. **Add the MySQL Connector/J to Your Project**:
   - **Right-click** on the project node in the Projects window.
   - Select **"Properties"** from the context menu.
   - In the Project Properties window, select the **"Libraries"** category on the left.
   - Click on the **"Add Library..."** button.
   - If the MySQL Connector/J is not listed, click **"Create..."** to add a new library.

4. **Create a New Library (if needed)**:
   - In the **"Library Manager"** dialog, click **"New Library..."**.
   - Enter a name for the library, such as **"MySQL Connector/J"**.
   - Set **Library Type** to **Class Library**.
   - Click **"OK"** to create the new library.

5. **Add JAR File to the Library**:
   - With the new library selected, click **"Add JAR/Folder"**.
   - Browse to the location where you extracted the MySQL Connector/J, select the `.jar` file, and click **"Open"**.

6. **Add the Library to Your Project**:
   - After creating and configuring the library, it will appear in the **"Add Library"** dialog.
   - Select your newly created library and click **"Add Library"** to include it in your project.
   - Click **"OK"** to close the Project Properties window.

## 3. Configure MySQL Connection

1. **Open the Services Tab**:
   - Go to the **"Services"** tab in NetBeans.

2. **Add a New Database Connection**:
   - Right-click on **"Databases"** and select **"New Connection..."**.

3. **Select MySQL Driver**:
   - In the **"New Connection"** dialog, select **"MySQL (Connector/J driver)"** as the database driver.

4. **Enter Database Connection Details**:
   - **Database URL**: Enter the URL in the format `jdbc:mysql://hostname:port/database_name`.
   - **Username**: Enter your MySQL username.
   - **Password**: Enter your MySQL password.
   - Click **"Test Connection"** to ensure everything is configured correctly.

5. **Finish Configuration**:
   - Click **"Finish"** to create the connection.

## 4. Use MySQL in Your Code

- Now you can use JDBC to interact with your MySQL database from your Java code. Here’s a simple example of how to establish a connection:

    ```java
    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.SQLException;

    public class DatabaseConnection {
        public static void main(String[] args) {
            String url = "jdbc:mysql://localhost:3306/your_database";
            String user = "your_username";
            String password = "your_password";
            
            try {
                Connection conn = DriverManager.getConnection(url, user, password);
                System.out.println("Connected to the database!");
                // Perform database operations
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    ```

## Summary

1. **Download and Extract** the MySQL Connector/J.
2. **Add** the `.jar` file to NetBeans as a library.
3. **Configure** the database connection using NetBeans' Services tab.
4. **Use** JDBC in your Java code to interact with MySQL.

By following these steps, you’ll have the MySQL Connector/J properly installed and configured for use in your NetBeans projects.

