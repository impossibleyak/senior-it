== Local Date Time Java ==

Importing LocalDateTime
Use :
    java.time.* //rather than using 
    java.time.DateTime
    
Do not name your class or package LocalDateTime or LocalDate or LocalTime

A guide is available in the Java Docs Api

=== Constructor ===
Because this class is a factory method pattern class the constructor varies.
It does not call the 'new' keywords but instead use the .of pattern
Example:
    
   NameOfTheClass NameOfTheObject = NameofTheClass.of(input parameters); 
   LocalDateTime a2 = LocalDateTime.of();
   
   
=== Duration ==
When creating a Duration object the method Duration.ofHours(999999) helped for cycle race class.
