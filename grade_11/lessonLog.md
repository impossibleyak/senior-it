
lessonNum: 1
date:
grade: 11
subject: IT
content: pg 57 textbook
+Review the following terms
+methods
+classes
+OOP
+fields
+variables
+Encapsulation
+new keywords as well as static void public
+Complete activity 1,2,3

lessonNum: 2
date:
grade: 11
subject: IT
content: pg 62 67
+Primitives
+constuctors
+field calls vs methods
+Parameterised constructor

lessonNum: 3
date:
grade: 11
subject: IT
content: Pg 68 
+Private fields
+getter and setters
+Void methods
+To String method

lessonNum: 4
date:
grade 11
subject: IT
content: 57 61 pg 
Review objects classes

lessonNum: 5
date:
grade 11
subject: IT
content: Review understanding of two classes and what an object has and does. Calling to other class changing properties of the object in the ether, the main method and its place, building and running. Packages, capital letters.
+Start constructor - how does Java know how to make Line objects

lessonNum: 6
date:
grade 11
subject: IT 
content: Activity 4,5,6

lessonNum: 7
date:
grade 11
subject: IT
content: Review constructor with paramters 
+Review private

lessonNum: 8
date:
grade 11
subject: IT
content: Complete setters and review reasons for needing them.

lessonNum: 9
date:
grade 11
subject: IT
content: complete setters and helper methods
+review reasons for them
+Activity 8 and 9

lessonNum: 10
grade 11
subject: IT
date: 2024-01-31
content: Completed another practise on OOP
+Kids dont feel confident yet.

lessonNum: 11
date: 2024-02-01
grade 11
subject: IT
content: Discussed the keywords void static and typed methods using two class setup.
+Refered to char JavaDocs
+Started countLetterDigits page 5
+Q5 -> boolean testing for isLetter
+Q6 -> Understanding Ascii table
 
lessonNum: 12
date: 2024-02-02
grade: 11
subject: IT
content:Completed practise act on page 5 and other 3 questions for homework
pg 15 Ex2

lessonNum: 13
date:
grade: 11
subject: IT024-02-06
content: Completed further practise activities in the char/string class
+pg 19 Q 3 4 6
+5 and 6 also useful

lessonNum: 14
date: 2024-02-06
grade: 11
subject: IT
content: Marks the checkpoint from theory
+Discuss chars and int's in netbeadn
+Pg 41 
+Activity one


lessonNum: 15
date: 
grade: 11
subject: IT
content: Still completing task
review string index and charAt
And substring()
talked about objects and static

lessonNum: 16
date: 
grade: 11
subject: IT
content: Review string equality and string methods
Add new excersizes Q2Q3

lessonNum: 17
date: 
grade: 11
subject: IT
content: FREE LESSON TO PLAY GO SCOTTIE

lessonNum: 18
date:
grade: 11
subject: IT
content: Started talking about paper classroom computer
Worked up to CACHE in the text book

lessonNum: 19
date: 
grade: 11
subject: IT
content: worked theory pg 42 - 37
did checkpoint 1
mark next lesson


lessonNum: 20
date: essonNum/ndate:/ncontent:/ngrade:/nsubject:
grade: 11
subject: IT
content: Start scanner and review tokens
+Start q1

lessonNum: 21
date: essonNum/ndate:/ncontent:/ngrade:/nsubject:
grade: 11
subject: IT
content: Review scanner mark Q1
+start Q2 while loop

lessonNum: 22
date:
grade: 11
subject: IT
content: I screwed up calling next() in the while loop rather than writing to a word variable
+Review adding files in File object
+Two seperate pieces and throws IOException

lessonNum: 23
date:
grade: 11
subject: IT
content: Completed ReadNames2 and ReadNums2

lessonNum: 24
date: 
grade: 11
subject: IT
content: Review basic patterns to learn scanner object.
+MAKE notes in folder for pattern order.
+Students still stumped, we did BirthDates


lessonNum: 25
date: essonNum/ndate:/ncontent:/ngrade:/nsubject:
grade: 11
subject: IT
content: Continued using the scanner class.
+Students were much more comfortable working on their own
+Did the question Extract(uses a third scanner, which does not work on UNIX) but should on windows



