
/* Programming Taxonomy

Level 1 - declare variables, instantiate object with no parameters, input, output,
increment, decrement, casting types

Level 2 - create and instantiate object with parameters, calculations, casting
types, if statement with one condition for loops and while loops, known algorithms
(sorting and searching), generate a random number, code simple (no loops or if
statements) void methods in a class with no parameters

Level 3 - Arrays of objects, methods with loops or if statements, code typed/void
methods with parameters and call them correctly. Static and final fields.
*/
import java.time.*;

public class ExtraTime 
{
    public static void main(String[] args)
    {
        int startHour = 10;
        int startMin = 30;

        int endHour = 12;
        int endMins = 30;
  

        LocalTime start = LocalTime.of(startHour, startMin);
        LocalTime end = LocalTime.of(endHour, endMins);
        Duration diff = Duration.between(start,end);
        System.out.print(diff);




    }
}


