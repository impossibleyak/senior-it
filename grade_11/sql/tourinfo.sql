#
# DUMP FILE
#
# Database is ported from MS Access
#------------------------------------------------------------------
# Created using "MS Access to MySQL" form http://www.bullzip.com
# Program Version 5.5.282
#
# OPTIONS:
#   sourcefilename=C:/Users/DeliaKench/OneDrive - Funworks/Documents/TourInfo.mdb
#   sourceusername=
#   sourcepassword=
#   sourcesystemdatabase=
#   destinationdatabase=TourInfo
#   storageengine=InnoDB
#   dropdatabase=0
#   createtables=1
#   unicode=1
#   autocommit=1
#   transferdefaultvalues=1
#   transferindexes=1
#   transferautonumbers=1
#   transferrecords=1
#   columnlist=1
#   tableprefix=
#   negativeboolean=0
#   ignorelargeblobs=0
#   memotype=LONGTEXT
#   datetimetype=DATETIME
#

CREATE DATABASE IF NOT EXISTS `TourInfo`;
USE `TourInfo`;

#
# Table structure for table 'tblTourists'
#

DROP TABLE IF EXISTS `tblTourists`;

CREATE TABLE `tblTourists` (
  `FlightNoOut` VARCHAR(255), 
  `OutDate` DATE, 
  `FlightNoreturn` VARCHAR(255), 
  `ReturnDate` DATE, 
  `Team` VARCHAR(255), 
  `Surname` VARCHAR(255), 
  `FirstName` VARCHAR(255), 
  `ID` VARCHAR(255), 
  `CelNo` VARCHAR(255), 
  `TotalTourCost` DOUBLE NULL, 
  `PaidTourCost` DOUBLE NULL, 
  `PriorityBoarding` TINYINT(1) DEFAULT 0, 
  INDEX (`ID`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'tblTourists'
#

INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-08-05', '5Z201', '2023-08-06', 'Rooibos', 'Pienaar', 'Francois', '930423 0038 082', '0824782523', 5000, 1500, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('GE123', '2023-08-09', 'GE315', '2023-08-11', 'Fynbos', 'Barnard', 'Roy', '920925 0392 089', '0837601611', 5000, 1500, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-08-13', 'FA475', '2023-08-15', 'Lavender', 'Terblanche', 'Eddy', '910226 5040 084', '0761326564', 7500, 1000, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-08-17', '5Z201', '2023-08-20', 'Marula', 'Smit', 'John', '910905 0021 088', '0726088803', 4000, 0, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('5Z101', '2023-08-21', 'FA475', '2023-08-25', 'Rooibos', 'Slater', 'Gerald', '900213 5373 006', '0823032045', 7500, 500, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-08-25', 'GE315', '2023-08-27', 'Rooibos', 'Anelke', 'Nicolas', '881206 5043 085', '0766619526', 7500, 700, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-08-29', 'FA475', '2023-08-31', 'Ginger', 'Daniel', 'Mark', '930323 5237 087', '0794271614', 4000, 1500, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('GE123', '2023-09-02', 'GE315', '2023-09-05', 'Marula', 'Stevens', 'Ettienne', '900806 0130 089', '0763423525', 4000, 0, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-09-06', 'FA475', '2023-09-08', 'Fynbos', 'Van Zyl', 'Evan', '900111 0121 083', '0768340154', 5000, 0, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('5Z101', '2023-09-10', 'FA475', '2023-09-13', 'Ginger', 'Laidlaw', 'Daniel', '910215 5052 082', '0729549741', 7500, 750, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('GE123', '2023-09-14', 'FA475', '2023-09-15', 'Rooibos', 'Le Grange', 'Donald', '891222 0072 084', '0828906660', 4000, 800, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-09-18', 'FA475', '2023-09-21', 'Lavender', 'Fourie', 'Jaques', '930630 0014 083', '0762214526', 5000, 2000, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-09-22', 'FA475', '2023-09-26', 'Ginger', 'Prince', 'Neville', '920116 5204 087', '0795764557', 4500, 1000, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('GE123', '2023-09-26', 'FA475', '2023-09-28', 'Marula', 'Walker', 'Matthew', '920430 5020 082', '0764614178', 7500, 0, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-09-30', 'FA475', '2023-10-03', 'Lavender', 'Hosking', 'Oregan', '930611 5252 084', '0821744769', 4500, 0, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-10-04', 'FA475', '2023-10-07', 'Ginger', 'De Villiers', 'Div', '920424 5286 082', '0839277637', 4000, 1250, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-10-08', 'FA475', '2023-10-10', 'Fynbos', 'Forlan', 'Diego', '930430 5027 085', '0729578295', 4500, 700, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('GE123', '2023-10-12', 'FA475', '2023-10-14', 'Marula', 'Du Plessis', 'Fanie', '910130 0251 086', '0821340604', 5000, 600, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-10-16', 'FA475', '2023-10-18', 'Fynbos', 'Edwards', 'Russel', '910621 5008 083', '0838977425', 4000, 0, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('5Z101', '2023-10-20', '5Z201', '2023-10-22', 'Ginger', 'Steyn', 'Francois', '900826 0083 005', '0764190033', 4500, 1200, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-10-24', '5Z201', '2023-10-29', 'Ginger', 'Samuel', 'Peter', '900326 0199 083', '0723216129', 5000, 950, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-10-28', 'FA475', '2023-10-30', 'Lavender', 'Torres', 'Fernando', '910812 5048 084', '0825503159', 4500, 800, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-11-01', 'FA475', '2023-11-04', 'Fynbos', 'Kelly', 'Rose', '890414 5038 084', '0762426044', 7500, 1200, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-11-05', 'FA475', '2023-11-06', 'Lavender', 'Daddy', 'Puff', '920526 0020 082', '0794076535', 4000, 0, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-11-09', 'FA475', '2023-11-11', 'Rooibos', 'Strydom', 'Dane', '910918 5210 085', '0729970328', 4000, 1000, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-11-13', '5Z201', '2023-11-15', 'Ginger', 'Van den Bergh', 'Donovan', '891229 5089 088', '0828492349', 5000, 1200, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-11-17', '5Z201', '2023-11-18', 'Fynbos', 'Potgieter', 'Tanille', '901216 0235 087', '0761264032', 4000, 1500, 1);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('5Z101', '2023-11-21', 'GE315', '2023-11-25', 'Rooibos', 'Williamson', 'Graeme', '890626 5191 080', '0793962850', 7500, 800, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-11-25', 'FA475', '2023-11-28', 'Lavender', 'Poonsamy', 'Kevin', '910418 5107 084', '0764051908', 4500, 1500, 0);
INSERT INTO `tblTourists` (`FlightNoOut`, `OutDate`, `FlightNoreturn`, `ReturnDate`, `Team`, `Surname`, `FirstName`, `ID`, `CelNo`, `TotalTourCost`, `PaidTourCost`, `PriorityBoarding`) VALUES ('FA532', '2023-11-29', 'GE315', '2023-12-02', 'Marula', 'Van Kampen', 'Fanie', '921022 5011 085', '0825689216', 7500, 1000, 0);
# 30 records


