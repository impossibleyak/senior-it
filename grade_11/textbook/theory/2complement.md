== Steps to 2's complement ==

1. Write the number in binary as if it is a normal positive binary number.
2. Size the number according to the needed bits. Usually 8 bits(byte)
3. Invert all bits
4. Add 1 bit
5. Done

''' Example

-64
= - 0100 0000
=   1011 1111
+           1
=============
=   1100 0000

'''


